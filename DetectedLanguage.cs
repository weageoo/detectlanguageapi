﻿namespace DetectLanguageAPI
{
    public sealed class DetectedLanguage
    {
        public string Language { get; set; }
        public bool IsReliable { get; set; }
        public double Confidence { get; set; }
    }
}
