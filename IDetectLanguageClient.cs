using System.Collections.Generic;

namespace DetectLanguageAPI
{
    public interface ILanguageDetector
    {
        ICollection<ICollection<DetectedLanguage>> BatchDetectLanguages(params string[] textSamples);
        ICollection<DetectedLanguage> BatchDetectMostProbableLanguages(params string[] textSamples);
        DetectedLanguage BatchDetectMostProbableLanguage(params string[] textSamples);
        ICollection<DetectedLanguage> DetectLanguages(string textSample);
        DetectedLanguage DetectMostProbableLanguage(string textSample);
        DetectedLanguage DetectMostProbableLanguage(string text, string[] langsFilter);
    }
}