﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Remoting;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;

namespace DetectLanguageAPI
{
    public sealed class DetectLanguageClient : ILanguageDetector
    {
        #region Constants

        private const string WebProtocol = "http";
        private const string DefaultApiKey = "demo";
        private const string DefaultVersion = "0.2";
        private const string RequestFormatString = WebProtocol + "://ws.detectlanguage.com/{0}/detect?{2}key={1}";
        private const string RequestMultyParameterFormatString = "q[]={0}&";

        #endregion

        private readonly WebClient _webClient = new WebClient();
        private readonly DataContractJsonSerializer _serializer = new DataContractJsonSerializer(typeof(ResponseEntity));

        public string ApiKey { get; set; }
        public string Version { get; set; }

        public DetectLanguageClient(string version = DefaultVersion, string apiKey = DefaultApiKey)
        {
            if (string.IsNullOrEmpty(version))
                throw new ArgumentException("Param can not be null or empty.", "version");
            if (string.IsNullOrEmpty(apiKey))
                throw new ArgumentException("Param can not be null or empty.", "apiKey");

            Version = version;
            ApiKey = apiKey;
        }

        public ICollection<ICollection<DetectedLanguage>> BatchDetectLanguages(params string[] textSamples)
        {
            return BatchDetectSync(textSamples).Select(x => (ICollection<DetectedLanguage>) x.ToList()).ToList();
        }

        public ICollection<DetectedLanguage> BatchDetectMostProbableLanguages(params string[] textSamples)
        {
            return BatchDetectLanguages(textSamples).Select(GetMostProbable).ToList().AsReadOnly();
        }

        public DetectedLanguage BatchDetectMostProbableLanguage(params string[] textSamples)
        {
            return GetMostProbable(BatchDetectLanguages(textSamples).Select(GetMostProbable).ToList());
        }

        public ICollection<DetectedLanguage> DetectLanguages(string text)
        {
            return DetectSync(text).ToList().AsReadOnly();
        }

        public DetectedLanguage DetectMostProbableLanguage(string text, string[] langsFilter)
        {
            var langs = DetectSync(text).Where(x => langsFilter.Contains(x.Language)).ToList();
            var lang = GetMostProbable(langs);

            return lang;
        } 

        public DetectedLanguage DetectMostProbableLanguage(string text)
        {
            var langs = DetectSync(text).ToList();
            var lang = GetMostProbable(langs);

            return lang;
        }

        private static DetectedLanguage GetMostProbable(ICollection<DetectedLanguage> langs)
        {
            var mostReliableLang = langs.FirstOrDefault();
            if (mostReliableLang == null)
            {
                return null;
            }

            foreach (var lang in langs)
            {
                if (lang.Confidence > mostReliableLang.Confidence)
                {
                    mostReliableLang = lang;
                }
            }

            return mostReliableLang;
        }

        private IEnumerable<DetectedLanguage> DetectSync(string text)
        {
            return BatchDetectSync(text).SelectMany(x => x);
        }

        private IEnumerable<IEnumerable<DetectedLanguage>> BatchDetectSync(params string[] textSamples)
        {
            try
            {
                var sb = new StringBuilder();

                foreach (var text in textSamples)
                {
                    sb.AppendFormat(RequestMultyParameterFormatString, HttpUtility.UrlEncode(text));
                }

                var query = string.Format(RequestFormatString, Version, ApiKey, sb);
                var responseJSON = _webClient.DownloadString(query);
                var response = DeserializeResponse(responseJSON);

                if (response.Error != null)
                {
                    throw new RemotingException(
                        string.Format("Remote error: {0} [code {1}].", response.Error.Message, response.Error.Code));
                }
                if (response.Data == null)
                {
                    throw new Exception("Invalid response.");
                }

                var detectedLanguages =
                    response.Data.Detections.Select(x => x.Select(t => t.ToDetectedLanguage()));

                return detectedLanguages;
            }
            catch (NullReferenceException ex)
            {
                throw new Exception("Response deserialization error.", ex);
            }
        }

        #region Serialization Helpers

        [DataContract]
        internal sealed class ResponseEntity
        {
            [DataMember(Name = "data", IsRequired = false)]
            public DataEntity Data { get; set; }

            [DataMember(Name = "error", IsRequired = false)]
            public ErrorEntity Error { get; set; }
        }
        [DataContract]
        internal sealed class DataEntity
        {
            [DataMember(Name = "detections")]
            public IEnumerable<IEnumerable<DetectionEntity>> Detections { get; set; }
        }
        [DataContract]
        internal sealed class DetectionEntity
        {
            [DataMember(Name = "language")]
            public string Language { get; set; }

            [DataMember(Name = "confidence")]
            public double Confidence { get; set; }

            [DataMember(Name = "isReliable")]
            public bool IsReliable { get; set; }

            public DetectedLanguage ToDetectedLanguage()
            {
                return
                    new DetectedLanguage
                    {
                        Language = Language,
                        Confidence = Confidence,
                        IsReliable = IsReliable
                    };
            }
        }
        [DataContract]
        internal sealed class ErrorEntity
        {
            [DataMember(Name = "code")]
            public int Code { get; set; }

            [DataMember(Name = "message")]
            public string Message { get; set; }
        }

        private ResponseEntity DeserializeResponse(string json)
        {
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(json)))
            {
                var resp = (ResponseEntity)_serializer.ReadObject(ms);
                return resp;
            }
        }

        #endregion
    }
}